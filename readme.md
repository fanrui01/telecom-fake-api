# Fake APIs
This services acts as fake apis for all the telecom service providers.

## Function
It is a simple mock function. Provide endpoint that takes any request and return with a response containing the following information. 
- Success or not. This feature serves to simulate different service quality of success rate. Success rate is defined in **constants.py**. It is a random function. 
- It sends response with a random time (0-9s) of delay. This feature serves to simulate different service quality of latency. 

## Deployment
### 
https://uwt8pg91g2.execute-api.ap-southeast-1.amazonaws.com/dev/telecom/30/send/