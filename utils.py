import random
import math
import constants

def random_10():
    return math.floor(10 * random.random())


def pass_with_prob(fail_rate = constants.FAIL_RATE):
    return random.random() > fail_rate