from flask import Flask
import utils
import time

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "Works"


@app.route("/telecom/<telecom_id>/send/", methods=['GET', 'POST'])
def telecom_apis(telecom_id):
    info_to_return = {}
    if not utils.pass_with_prob():
        info_to_return["isSuccessful"] = False
        return info_to_return

    info_to_return["isSuccessful"] = True
    sleep_secs = utils.random_10()
    print("sleep seconds: ", sleep_secs)
    time.sleep(sleep_secs)
    return info_to_return